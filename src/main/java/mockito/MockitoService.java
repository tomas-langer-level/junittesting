package mockito;

import common.TestFilter;
import common.TestRecord;

import java.util.List;

/**
 * Service under test.
 * @author tomas.langer
 */
class MockitoService {

    private final MockitoDao mockitoDao;

    MockitoService(MockitoDao mockitoDao) {
        this.mockitoDao = mockitoDao;
    }

    public List<TestRecord> getRecordsWithEvenIds(TestFilter testFilter) {
        List<TestRecord> records = mockitoDao.getAll();
        filterRecords(testFilter, records);
        return records;
    }

    public String realMethodOfPartialMock() {
       return "REAL realMethodOfPartialMock value";
    }

    private void filterRecords(TestFilter testFilter, List<TestRecord> records) {
        records.removeIf(record -> !testFilter.hasEvenId(record));
    }

}
