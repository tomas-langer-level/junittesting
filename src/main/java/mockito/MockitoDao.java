package mockito;

import common.CommonDao;

/**
 * Test DAO simulating reading of records from database.
 * @author tomas.langer
 */
class MockitoDao extends CommonDao {

    int paramMethodExample(String param) {
        return 0;
    }

}
