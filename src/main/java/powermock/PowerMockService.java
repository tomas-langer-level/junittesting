package powermock;

import common.TestFilter;
import common.TestRecord;

import java.util.List;

/**
 * Service under test.
 * @author tomas.langer
 */
class PowerMockService {

    private static PowerMockService INSTANCE;
    private final PowerMockDao powerMockDao;

    private PowerMockService() {
        this.powerMockDao = PowerMockDao.getInstance();
    }

    // Static method to get singleton instance of this class.
    static synchronized PowerMockService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PowerMockService();
        }
        return INSTANCE;
    }

    List<TestRecord> getRecordsWithEvenIds(TestFilter testFilter) {
        List<TestRecord> records = powerMockDao.getAll();
        filterRecords(testFilter, records);
        return records;
    }

    String realMethodOfPartialMock() {
        return "REAL realMethodOfPartialMock value";
    }

    private void filterRecords(TestFilter testFilter, List<TestRecord> records) {
        records.removeIf(record -> !testFilter.hasEvenId(record));
    }

    public int privateMethodDelegate() {
        return privateMethod();
    }

    private int privateMethod() {
        return 0;
    }

}
