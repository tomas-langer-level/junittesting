package powermock;

import common.CommonDao;
import common.RecordType;

import java.util.ArrayList;
import java.util.List;

/**
 * Test DAO simulating reading of records from database.
 * @author tomas.langer
 */
class PowerMockDao extends CommonDao {

    private static PowerMockDao INSTANCE = null;

    private PowerMockDao() {
        // Private constructor to prevent external instantiation.
    }

    // Static method to get singleton instance of this class.
    public static synchronized PowerMockDao getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PowerMockDao();
        }
        return INSTANCE;
    }

    int paramMethodExample(String param) {
        return 0;
    }

}
