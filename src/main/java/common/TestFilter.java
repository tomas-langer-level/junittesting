package common;

/**
 * Filter for non database filtering in some service.
 * @author tomas.langer
 */
public class TestFilter {

    public boolean hasEvenId(TestRecord testRecord) {
        return testRecord.getId() % 2 == 0;
    }

}
