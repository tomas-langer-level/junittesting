package common;

import java.util.ArrayList;
import java.util.List;

/**
 * Common parent class of test DAO simulating reading of records from database.
 * @author tomas.langer
 */
public class CommonDao {

    /**
     * Gets list of all {@link TestRecord} from database.
     * @return
     */
    public List<TestRecord> getAll() {
        List<TestRecord> testRecords = new ArrayList<>();
        for (int i = 1 ; i < 10 ; i++) {
            testRecords.add(new TestRecord(i, "data" + i, RecordType.DATABASE));
        }
        return testRecords;
    }

}
