package common;

/**
 * Common enum with record types.
 * @author tomas.langer
 */
public enum RecordType {
    /** Simulated record from database. */
    DATABASE,
    /** Mocked record from JUnit test. */
    TEST
}
