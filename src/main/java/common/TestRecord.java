package common;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * One data record in database.
 * @author tomas.langer
 */
@Data
@AllArgsConstructor
public class TestRecord {
    private final int id;
    private final String data;
    private final RecordType type;
}
