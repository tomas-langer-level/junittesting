package mockito;

import common.RecordType;
import common.TestFilter;
import common.TestRecord;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * @author tomas.langer
 */
@RunWith(MockitoJUnitRunner.class)
public class MockitoServiceTest {

    private static final String FAKE_VALUE_OF_REAL_METHOD = "FAKE realMethodOfPartialMock value";

    @Mock
    private MockitoDao mockitoDao;
    private MockitoService mockitoService;
    private static final List<TestRecord> records = new ArrayList<>();

    @BeforeClass
    public static void init() {
        for (int i = 1 ; i < 10 ; i++) {
            records.add(new TestRecord(i, "data" + i, RecordType.TEST));
        }
    }

    @Before
    public void setUp() {
        when(mockitoDao.getAll()).thenReturn(records);
        mockitoService = new MockitoService(mockitoDao);
        // Test of partial mock
        mockitoService = spy(mockitoService);
        when(mockitoService.realMethodOfPartialMock()).thenReturn(FAKE_VALUE_OF_REAL_METHOD);
        // Test of param method mock
        when(mockitoDao.paramMethodExample(Mockito.anyString())).thenReturn(1);
    }

    @Test
    public void testGetRecordsWithEvenIds() {
        List<TestRecord> records = mockitoService.getRecordsWithEvenIds(new TestFilter());
        assertFalse("No records", records.isEmpty());
        boolean allIsTest = records.stream().allMatch(record -> record.getType() == RecordType.TEST);
        assertTrue("All records isn't a type " + RecordType.TEST.name(), allIsTest);
        boolean allIsEven = records.stream().allMatch(record -> record.getId() % 2 == 0);
        assertTrue("All records ids isn't even", allIsEven);
        String mockedValue = mockitoService.realMethodOfPartialMock();
        assertEquals("Partial mock failed", FAKE_VALUE_OF_REAL_METHOD, mockedValue);
        int result = mockitoDao.paramMethodExample("blabla");
        assertEquals("Param method mock failed", 1, result);
    }
}