package powermock;

import common.RecordType;
import common.TestFilter;
import common.TestRecord;
import org.easymock.Capture;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author tomas.langer
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({PowerMockDao.class, PowerMockService.class})
public class PowerMockServiceTest {

    private static final String FAKE_VALUE_OF_REAL_METHOD = "FAKE realMethodOfPartialMock value";

    private PowerMockDao powerMockDao;
    private PowerMockService powerMockService;
    private static final List<TestRecord> records = new ArrayList<>();
    private Capture<String> capturedArgument = Capture.newInstance();

    @BeforeClass
    public static void init() {
        for (int i = 1; i < 10; i++) {
            records.add(new TestRecord(i, "data" + i, RecordType.TEST));
        }
    }

    @Before
    public void setUp() throws Exception {
        powerMockDao = EasyMock.createMock(PowerMockDao.class);
        EasyMock.expect(powerMockDao.getAll()).andReturn(records).anyTimes();
        // Capture method param value
        EasyMock.expect(powerMockDao.paramMethodExample(EasyMock.capture(capturedArgument))).andReturn(1);
        EasyMock.replay(powerMockDao);
        PowerMock.mockStatic(PowerMockDao.class);
        // Mock of public static method PowerMockDao#getInstance() I know method name PowerMock.expectPrivate is misleading
        // but true is this method also works with static methods no matter about the access modifier.
        PowerMock.expectPrivate(PowerMockDao.class, "getInstance").andReturn(powerMockDao);
        PowerMock.replay(PowerMockDao.class);
        // Create partial mock of PowerMockService to mock realMethodOfPartialMock() method.
        powerMockService = PowerMock.createPartialMock(PowerMockService.class, "realMethodOfPartialMock", "privateMethod");
        PowerMock.expectPrivate(powerMockService, "realMethodOfPartialMock").andReturn(FAKE_VALUE_OF_REAL_METHOD).anyTimes();
        // Set value of PowerMockService#powerMockDao by reflection
        Field powerMockDaoField = PowerMockService.class.getDeclaredField("powerMockDao");
        powerMockDaoField.setAccessible(true);
        powerMockDaoField.set(powerMockService, powerMockDao);
        // Mock of private method
        PowerMock.expectPrivate(powerMockService, "privateMethod").andReturn(1).anyTimes();
        PowerMock.replay(powerMockService);
//        powerMockService = PowerMockService.getInstance();
    }

    @Test
    public void testGetRecordsWithEvenIds() {
        List<TestRecord> records = powerMockService.getRecordsWithEvenIds(new TestFilter());
        assertFalse("No records", records.isEmpty());
        boolean allIsTest = records.stream().allMatch(record -> record.getType() == RecordType.TEST);
        assertTrue("All records isn't a type " + RecordType.TEST.name(), allIsTest);
        boolean allIsEven = records.stream().allMatch(record -> record.getId() % 2 == 0);
        assertTrue("All records ids isn't even", allIsEven);
        String mockedValue = powerMockService.realMethodOfPartialMock();
        assertEquals("Partial mock failed", FAKE_VALUE_OF_REAL_METHOD, mockedValue);
        int result = powerMockService.privateMethodDelegate();
        assertEquals("Private method mock failed", 1, result);
        // Capture argument example
        powerMockDao.paramMethodExample("param");
        assertEquals("Argument capture failed", "param", capturedArgument.getValue());
    }

}